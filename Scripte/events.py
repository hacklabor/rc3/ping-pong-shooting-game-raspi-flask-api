#!/usr/bin/python3
import traceback

import socketio

speed = 5000
status = ''
allPoints = 0
aktPoints = 0
NewPlayer = True
FirstUser =False
NewShoot = False
NewPlayerReqTime = 0
PlayerCounter=40
ShootCounter = 0

UserName ="NONE1"

HighScoreName= ["NONE1","NONE2","NONE3","NONE4","NONE5","NONE6","NONE7","NONE8","NONE9","NONE10"]
HighScorePoints=["00000000","00000000","00000000","00000000","00000000","00000000","00000000","00000000","00000000","00000000"]

AimLastAliveTime = [0, 0, 0]
AimHIT =[0, 0, 0]
AimLastHitTime = 0
shootStatus = b'2'
firstRead = 0

import time
from secrets import MQTT_SERVER, key
from shoot import writeToESP
from sendGrbl import home, moveXY, moveX, moveY, hold, resetAlarm, grblINIT
import json
import paho.mqtt.client as mqtt
import os
import random
import hashlib
import base64
import datetime as dt

logfile = open("/home/pi/Scripte/log/logEventsPy", "w+")
logfile.write("")
logfile.close()


sio = socketio.Client()


def readHighScore() :
    global HighScorePoints
    global HighScoreName
    global ShootCounter
    if not os.path.isfile('/home/pi/HighScoreName'):
        fh=open("/home/pi/HighScoreName","w")
        for val in HighScoreName:
            fh.write(val+"\n")
        fh.close()
    if not os.path.isfile('/home/pi/HighScorePoints'):
        fh=open("/home/pi/HighScorePoints","w")
        for val in HighScorePoints:
            fh.write(val+"\n")
        fh.close()

    if not os.path.isfile('/home/pi/SHOOTS'):
        fh = open("/home/pi/SHOOTS", "w")
        fh.write(str(0))
        fh.close()

    fh = open("/home/pi/HighScoreName", "r")
    for i in range(len(HighScoreName)):
        HighScoreName[i] = fh.readline().strip("\n")
    fh.close()

    fh = open("/home/pi/HighScorePoints", "r")
    for i in range(len(HighScorePoints)):
        HighScorePoints[i] = fh.readline().strip("\n")
    fh.close()
    fh = open("/home/pi/SHOOTS", "r")
    ShootCounter = int(fh.read())
    fh.close()


def writeHighScore(points,name):
    global HighScorePoints
    global HighScoreName

    for p in range(len(HighScorePoints)):
        p1=len(HighScorePoints)-1-p
        if points>int(HighScorePoints[p1]):
            if p>0:
                HighScorePoints[p1+1]=HighScorePoints[p1]
                HighScoreName[p1 + 1] = HighScoreName[p1]

            HighScorePoints[p1]=str(points).zfill(8)
            HighScoreName[p1] = name
        else:
            break

    fh1 = open("/home/pi/HighScoreName", "w")
    export=[]
    for i1 in range(len(HighScoreName)):
        fh1.write(HighScoreName[i1]+"\n")
        export.append(HighScorePoints[i1]+" : "+HighScoreName[i1])
        log("HIGHSCORE", HighScorePoints[i1]+" : "+HighScoreName[i1])
    fh1.close()
    fh2 = open("/home/pi/HighScorePoints", "w")
    for i1 in range(len(HighScorePoints)):
        fh2.write(HighScorePoints[i1] + "\n")
    fh2.close()
    fh = open("/home/pi/SHOOTSges", "a+")
    text = dt.datetime.now().strftime('%d %H:%M:%S') + "    "
    text += str(ShootCounter) + "\n"
    fh.write(text)
    fh.close()
    fh = open("/home/pi/SHOOTS", "w")
    fh.write(str(ShootCounter))
    fh.close()

    sio.emit('HighScoreList', export)









def log(text1, text2):

    logfile1 = open("/home/pi/Scripte/log/logEventsPy", "a+")
    text = dt.datetime.now().strftime('%d %H:%M:%S')+"    "
    text += text1+": "
    text += text2+"\n"
    logfile1.write(text)
    logfile1.close()


@sio.event
def connect():
    log("SIO",'connection established')


@sio.event
def disconnect():
    log("SIO",'disconnected from server')


@sio.event
def connect_error(error_msg):
    log("SIO","connection failed:\n{}".format(error_msg))

@sio.event
def getHighscore():
    writeHighScore(0,"dummy")


@sio.event
def fire():
    global shootStatus
    global NewShoot
    NewShoot = True



@sio.event
def nextUser(data):
    global NewPlayer
    global NewPlayerReqTime
    global UserName
    # TODO NEW USER
    NewPlayerReqTime = time.time_ns()/1000/1000/1000
    NewPlayer = True

    UserName = data
    log("NextUser", str(data))

@sio.event
def move(direction):
    print('move event emitted')
    log("SIO PAN", str(direction["pan"]))
    log("SIO TILT",str(direction["tilt"]))
    # obj=json.loads(direction)
    # TODO: code that needs to be executed to move launcher
    moveXY(direction["pan"], direction["tilt"], speed)


def checkAIMStatus(AimH):
    global NewPlayerReqTime
    pc = 0

    for ah in AimH:
        pc += int(ah)
    print(pc)
    if pc == 0:
        fh = open("/mnt/ramdisk/hit", "w")
        fh.write("DONE")
        fh.close()

        return False
    else:
        fh = open("/mnt/ramdisk/hit", "w")
        fh.write("HIT")
        fh.close()
        return True


def on_connect(client1, userdata, flags, rc):
    log("MQTT","Connected with result code {}".format(str(rc)))



def on_disconnect(client1, userdata, flags, rc):
    if rc != 0:
        log("MQTT","Unexpected disconnection.")
        exit(0)


def on_message(client1, userdata, msg):
    log("MQTT",msg.topic + " " + str(msg.qos) + " " + str(msg.payload))

    global allPoints
    global aktPoints
    global status
    global firstRead
    global AimHIT
    global AimLastHitTime
    global AimLastAliveTime

    try:
        if msg.topic == "sensor/Status":
            status = msg.payload.decode('utf-8')
            key, value = status.split(":")
            if key == "HIT":
                AimHIT[int(value)-1] = 1
                AimLastHitTime=time.time_ns()/1000/1000
            if key == "DONE":
                allPoints += aktPoints
                AimHIT[int(value)-1] = 0
            if key == "ALIVE":
                AimLastAliveTime[int(value)-1] = time.time_ns()/1000/1000/1000

        if msg.topic == "sensor/Points":
            if firstRead == 1:
                aktPoints = int(msg.payload.decode('utf-8'))
                fp1 = open("/mnt/ramdisk/points", "w")
                fp1.write("+" + (str(aktPoints + allPoints).zfill(8)))
                fp1.close()
            firstRead = 1

        checkAIMStatus(AimHIT)

    except json.JSONDecodeError as e:
        log("MQTT","Error decoding MQTT message: {}\n{}".format(e, msg.payload))

    except KeyError as e:
        log("MQTT","MQTT message did not contain required key: {}\n{}".format(e, msg.payload))

    except AssertionError as e:
        log("MQTT","MQTT message incorrect: {}\n{}".format(e, msg.payload))

    except Exception:
        log("MQTT",traceback.format_exc())


def on_log(client1, mqttc, obj, level, string):
    pass
    #print(string)

log("Allgemein","Start")

f = open("/mnt/ramdisk/hit", "w")
f.write("DONE")
f.close()

f = open("/mnt/ramdisk/token", "w")
f.write('helloWorld')
f.close()

f = open("/mnt/ramdisk/status", "w")
f.write('NONE')
f.close()
f = open("/mnt/ramdisk/time", "w")
f.write("0")
f.close()

f = open("/mnt/ramdisk/points", "w")
f.write("+" + (str(aktPoints + allPoints).zfill(8)))
f.close()
log("Allgemein","writeDefaultServerFiles: OK")


readHighScore()

grblINIT()
res = home().decode('utf-8').split(":")
print(res[0])
if  res[0]=="ALARM":
    resetAlarm()
    res = home().decode('utf-8').split(":")
    print(res[0])
    res = home().decode('utf-8').split(":")
    if res[0]=="ALARM":
        exit()


moveXY(50, 50, speed)

sio.connect('https://virushunt.hacklabor.de:3000')

hostname = MQTT_SERVER  # add secrets.py file to project dir with content: MQTT_SERVER = '<example>'
response = 1

# Test connection to MQTT-Broker
while response != 0:
    log("MQTT","Trying to reach MQTT-Broker at {}...".format(hostname))
    response = os.system("ping -c 1 " + hostname + " > /dev/null")

    # and then check the response...
    if response != 0:
        log("MQTT",hostname+'is Down! Retrying in 5 seconds...')
        time.sleep(5)
log("MQTT",'MQTT-Broker at {} is available.'.format(hostname))
# MQTT-Client setup
client = mqtt.Client()
client.on_connect = on_connect
client.on_disconnect = on_disconnect
client.on_message = on_message

client.connect(hostname, 1883, 60)
client.subscribe("sensor/#", 0)

client.loop_start()

oldShootStatus = b'0'
counter = 0
counter1 =0
shotttimeout=3
shNotaktiv=True
while 1 == 1:
    now = time.time_ns() / 1000 / 1000 / 1000


    if NewPlayer and ((now-NewPlayerReqTime) > 4.0  and  shNotaktiv ):

        if not checkAIMStatus(AimHIT) :

            txt = ''

            for i in range(0, 32):
                txt += chr(random.randint(20, 126))
                # md5 hash wegen verbotener Zeichen in der URL
            result = hashlib.md5(txt.encode())
            token = result.hexdigest()
            ft = open("/mnt/ramdisk/token", "w")
            ft.write(token)
            ft.close()
            encodeUrl = ''
            for i in range(0, 32):
                encodeUrl += chr((ord(token[i]) ^ ord(key[i])))
            base64_bytes = base64.b64encode(encodeUrl.encode('utf-8'))
            tokenAsBase64 = base64_bytes.decode('utf-8')
            writeHighScore(allPoints, UserName)
            ret = [tokenAsBase64, allPoints]
            PlayerCounter +=1
            if PlayerCounter > 40:
                PlayerCounter=0
                grblINIT()
                res = home().decode('utf-8').split(":")
                print(res[0])
                if res[0] == "ALARM":
                    resetAlarm()
                    res = home().decode('utf-8').split(":")
                    print(res[0])
                    res = home().decode('utf-8').split(":")
                    if res[0] == "ALARM":
                        exit()

                moveXY(50, 50, speed)
            log("SIO", "Next User Return")
            sio.emit('nextUserReturn', ret)
            allPoints = 0
            aktPoints = 0
            fp = open("/mnt/ramdisk/points", "w")
            fp.write("+" + (str(aktPoints)).zfill(8))
            fp.close()
            f = open("/mnt/ramdisk/time", "w")
            f.write(str(time.time_ns()))
            f.close()
            NewPlayer = False


    if NewShoot:
        shNotaktiv = False
        NewShoot = False
        log("SIO", 'fire event emitted')
        # TODO: code that needs to be executed to fire ball

        log("Shoot", str(writeToESP(100, 100, 0, 0, 0, 0)))
        shootStatus = '0'
        ff = open("/mnt/ramdisk/status", "w")
        ff.write("RELOAD")
        ff.close()
        ShootCounter += 1
        sio.emit('shootReload')
        counter = 0


    if counter > 100:
        shNotaktiv = True
        counter = 0

        f1 = open("/mnt/ramdisk/status", "w")
        f1.write("NONE")
        f1.close()
        sio.emit('shootDone')

    counter += 1
    pointC = 0
    for ah in AimHIT:
        pointC += int(ah)
    if now-AimLastHitTime > 60.0 and pointC > 0:
        log("AIM","Count Timeout")
        for i in range(len(AimHIT)):
            AimHIT[i] = 0
        checkAIMStatus(AimHIT)
    if counter1>200:
        counter1 =0
        aimAlive = [0, 0, 0]  # [links, rechts, mitte]
        for i in range(len(AimLastAliveTime)):
            if now - AimLastAliveTime[i] < 25.0:
                aimAlive[i] = 1
            else:
                log("AIM", "DEAD AIM " + str(i))
        sio.emit('AimAlive', aimAlive)
    counter1 += 1
    time.sleep(0.05)
