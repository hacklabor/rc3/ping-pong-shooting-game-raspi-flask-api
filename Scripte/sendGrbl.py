
import serial
import time
import datetime as dt

logfile = open("/home/pi/Scripte/log/logSendGrblPy", "w+")
logfile.write("")
logfile.close()


maxY=180.0
minY=50.0
maxX=35.0
minX=10.0
# Open grbl serial port
s = serial.Serial('/dev/ttyUSB1',115200)


def log(text1, text2):
	logfile1 = open("/home/pi/Scripte/log/logSendGrblPy", "a+")
	text = dt.datetime.now().strftime('%d %H:%M:%S') + "    "
	text += text1 + ": "
	text += text2 + "\n"
	logfile1.write(text)
	logfile1.close()


def grblINIT():
	s.write(b"\r\n\r\n")
	log("grbl", "INIT")
	time.sleep(2) 
	s.flushInput()  # Flush startup text in serial input


def home():
	s.flushInput()
	s.write(b"$H\r\n")
	ret= (s.readline())
	log("HOME", ret.decode("utf-8"))
	s.write(b"G92 X30 Y0 Z0\r\n")
	s.readline()
	s.write(b"G90 G21\r\n")
	s.readline()
	return ret

def resetAlarm():
	s.write(b"$X\r\n")
	ret= (s.readline())
	s.write(b"G91 G1 Y180 F2000 \r\n")
	ret= (s.readline())
	time.sleep(2)
	s.write(b"G90 G21\r\n")
	ret= (s.readline())
	return ret

def scale(val,min1,max1):
	return float(val)*float(max1-min1)/100.0+float(min1)

def moveX(val,speed):
	txt="G1 "
	txt+="X"
	txt+=f"{scale(val,minX,maxX)*(1):3.3f}"
	txt+=" F"
	txt+=str(speed)
	txt+="\r\n"
	log("WRITE",txt)
	s.write(txt.encode('utf-8'))  
	ret= (s.readline())
	log("RECV",ret.decode("utf-8"))
	return ret

def moveY(val,speed):
	txt="G1 "
	txt+="Y"
	txt+=f"{scale(val,minY,maxY):3.3f}"
	txt+=" F"
	txt+=str(speed)
	txt+="\r\n"
	log("WRITE",txt)
	s.write(txt.encode('utf-8')) 
	ret= (s.readline())
	log("RECV",ret.decode("utf-8"))
	return ret

def moveXY(valX,valY,speed):
	txt="G1 "
	txt+="X"
	txt+=f"{scale(valX,minX,maxX):3.3f}"
	txt+=" Y"
	txt+=f"{scale(valY,minY,maxY):3.3f}"
	txt+=" F"
	txt+=str(speed)
	txt+="\r\n"
	log("WRITE",txt)
	s.write(txt.encode('utf-8'))  
	ret= (s.readline())
	log("RECV",ret.decode("utf-8"))
	return ret

def hold():
	s.write(b"!")
	print("!")
	ret= (s.readline())
	log("RECV",ret.decode("utf-8"))
	return ret




  
