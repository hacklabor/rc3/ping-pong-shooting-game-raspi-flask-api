import base64
from secrets import key
import random
import hashlib


txt=''

for i in range(0,32):
    txt+=chr(random.randint(20, 126))
#md5 hash wegen verbotener Zeichen in der URL
result = hashlib.md5(txt.encode()) 
token = result.hexdigest() 
print('token: ',token) #token

encodeUrl=''
for i in range(0,32):
   encodeUrl+=chr((ord(token[i])^ord(key[i])))

print('verslüsselt: ',encodeUrl)

base64_bytes = base64.b64encode(encodeUrl.encode('utf-8'))

string1 = base64_bytes.decode('utf-8')

print ('base64 (soket.IO): ',string1) # token to send

base64_bytes = base64.b64decode(string1.encode('utf-8'))

decodStr=base64_bytes.decode('utf-8')

decToken=""

for i in range(0,32):
    decToken +=chr((ord(decodStr[i])^ord(key[i])))

print('decodiertes token: ', decToken)
