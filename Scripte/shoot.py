import serial
import time

import datetime as dt

logfile = open("/home/pi/Scripte/log/logShootPy", "w+")
logfile.write("")
logfile.close()

connected = False
reloaded = False
port = '/dev/ttyUSB0'
baud = 115200
serial_port = serial.Serial(port, baud, timeout=1)


def log(text1, text2):
    logfile1 = open("/home/pi/Scripte/log/logShootPy", "a+")
    text = dt.datetime.now().strftime('%d %H:%M:%S') + "    "
    text += text1 + ": "
    text += text2 + "\n"
    logfile1.write(text)
    logfile1.close()


def checkValue(val, min, max):
    if val < min:
        print("Wert " + str(val) + " zu Klein")
        return False
    if val > max:
        print("Wert " + str(val) + " zu groß")
        return False
    return True


def writeToESP(M1, M2, M1_dir, M2_dir, Fire, request):
    if not checkValue(M1, 0, 100): return
    if not checkValue(M2, 0, 100): return
    if not checkValue(M1_dir, 0, 1): return
    if not checkValue(M2_dir, 0, 1): return
    txt = '#,'
    txt += f'{M1:03d}' + ','
    txt += f'{M2:03d}' + ','
    txt += f'{M1_dir:01d}' + ','
    txt += f'{M2_dir:01d}' + ','
    txt += f'{Fire:01d}' + ','
    txt += f'{request:01d}' + ','
    txt += '#\n'
    log("send", txt)
    serial_port.write(txt.encode('utf-8'))
    recv = serial_port.readline()
    log("recv", recv.decode("utf-8"))
    ret = b''
    ret = recv.split(b",")
    if len(ret) == 7:
        return ret[5]

    return -1
