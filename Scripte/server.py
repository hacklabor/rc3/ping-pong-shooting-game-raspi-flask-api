#!/usr/bin/python3
# Web streaming example
# Source code from the official PiCamera package
# http://picamera.readthedocs.io/en/latest/recipes2.html#web-streaming

import io
import picamera
import logging
import socketserver
from threading import Condition
from http import server
import ssl
import datetime as dt
import numpy as np
import cv2
import os, time
from secrets import ffmpeg_url

from PIL import Image, ImageDraw, ImageFont, ImageChops



# BILDFORMAT
x = 640
y = 480

# Bild Offset
imXoff = 10
imYoff = 10
zoom = (0.0, 0.0, 1, 1)
# zoom=(0.5,0.5,0.25,0.25)
# AIM CIRCLE Duchmesser in Pixel
ac = 300
acFree = 50
acColor = 'grey'
acWidth = 4

PAGE = """\
<html>
<head>
<title>Virus Hunt Camera</title>
</head>
<body>
<center><h1>Virus Hunt Camera</h1></center>
<center><img src="stream.mjpg" width="640" height="480"></center>
</body>
</html>
"""


def log(logfile1,text1, text2):
    text = dt.datetime.now().strftime('%Y-%m-%d %H:%M:%S')+"    "
    text += text1+": "
    text += text2+"\n"
    logfile1.write(text)


class StreamingOutput(object):
    def __init__(self):
        self.frame = None
        self.token = None
        self.buffer = io.BytesIO()
        self.condition = Condition()
        self.mod_datePoints = os.stat("/mnt/ramdisk/points")[8]
        self.mod_dateToken = os.stat("/mnt/ramdisk/token")[8]
        self.mod_dateHit = os.stat("/mnt/ramdisk/hit")[8]
        self.mod_dateStatus = os.stat("/mnt/ramdisk/status")[8]
        self.mod_dateTime=os.stat("/mnt/ramdisk/time")[8]
        f = open("/mnt/ramdisk/points", "r")
        self.points = f.read()
        f.close()
        f = open("/mnt/ramdisk/token", "r")
        self.token = f.read().replace("\n", "")
        f.close()
        f = open("/mnt/ramdisk/hit", "r")
        self.hit = f.read()
        f.close()
        f = open("/mnt/ramdisk/status", "r")
        self.status = f.read()
        f = open("/mnt/ramdisk/time", "r")
        self.EndTime = int(f.read())
        self.shoot = 0


    def write(self, buf):
        if buf.startswith(b'\xff\xd8'):
            # New frame, copy the existing buffer's content and notify all
            # clients it's available
            self.buffer.truncate()
            with self.condition:
                mod_dateTokenNew = os.stat("/mnt/ramdisk/token")[8]
                if self.mod_dateToken != mod_dateTokenNew:
                    self.mod_dateToken = mod_dateTokenNew
                    f = open("/mnt/ramdisk/token", "r")
                    self.token = f.read().replace("\n", "")
                    f.close()


                self.frame = self.buffer.getvalue()
                # now add timestamp to jpeg
                # Convert to PIL Image
                cv2.CV_LOAD_IMAGE_COLOR = 1  # set flag to 1 to give colour image
                self.npframe = np.frombuffer(self.frame, dtype=np.uint8)
                # print(self.npframe)
                # print(len(self.npframe))
                if len(self.npframe) > 0:
                    self.pil_frame = cv2.imdecode(self.npframe, cv2.CV_LOAD_IMAGE_COLOR)
                    self.cv2_im_rgb = cv2.cvtColor(self.pil_frame, cv2.COLOR_BGR2RGB)
                    pil_im = Image.fromarray(self.cv2_im_rgb)
                    pil_im=ImageChops.offset(pil_im, imXoff,imYoff)
                    mod_datePointsNew = os.stat("/mnt/ramdisk/points")[8]
                    if self.mod_datePoints != mod_datePointsNew:
                        self.mod_datePoints = mod_datePointsNew
                        f = open("/mnt/ramdisk/points", "r")
                        self.points = f.read()
                        f.close()
                    mod_dateTimeNew = os.stat("/mnt/ramdisk/time")[8]
                    if self.mod_dateTime != mod_dateTimeNew:
                        self.mod_dateTime = mod_dateTimeNew
                        f = open("/mnt/ramdisk/time", "r")
                        self.EndTime = int(f.read())+(120*1000*1000*1000)
                        self.shoot = 5
                        f.close()
                    mod_dateHitNew = os.stat("/mnt/ramdisk/hit")[8]
                    if self.mod_dateHit != mod_dateHitNew:
                        self.mod_dateHit = mod_dateHitNew
                        f = open("/mnt/ramdisk/hit", "r")
                        self.hit = f.read()
                        f.close()
                    mod_dateStatusNew = os.stat("/mnt/ramdisk/status")[8]
                    if self.mod_dateStatus != mod_dateStatusNew:
                        self.mod_dateStatus = mod_dateStatusNew
                        f = open("/mnt/ramdisk/status", "r")
                        self.status = f.read()
                        if self.status == "RELOAD":
                            self.shoot -= 1
                        f.close()
                    font = ImageFont.truetype("/usr/share/fonts/truetype/digital-7/digital7Mono.ttf", 50)

                    color = 'rgb(128,255,0)'
                    BackGround_img1 = Image.new('RGB', (640, 90), "black")
                    BackGround_img2 = Image.new('RGB', (170, 480), "black")
                    pil_im.paste(BackGround_img1, (0, 0))
                    pil_im.paste(BackGround_img1, (0, 480 - 90))
                    pil_im.paste(BackGround_img2, (0, 0))
                    pil_im.paste(BackGround_img2, (640 - 170, 0))
                    DIFF = int((self.EndTime - time.time_ns()) / 1000 / 1000 / 1000)
                    if DIFF < 0:
                        DIFF = 0
                    myText = "Shoots: "+str(self.shoot)+"       Time: " + str(DIFF).zfill(3)
                    if self.hit == "HIT":
                        col = color
                    elif self.status == "RELOAD":
                        col = 'yellow'
                    else:
                        col = 'gray'
                    draw_pil_im = ImageDraw.Draw(pil_im)
                    draw_pil_im.ellipse((x / 2 - 250, y / 2 - 250, x / 2 + 250, y / 2 + 250), width=100,
                                        outline="black")
                    draw_pil_im.ellipse((x / 2 - 160, y / 2 - 160, x / 2 + 160, y / 2 + 160), width=10, outline=col)
                    draw_pil_im.text((x / 2 - 115, 5), self.points, fill=color, font=font)
                    draw_pil_im.text((20, 480 - 50), myText, fill=color, font=font)
                    draw_pil_im.line((x / 2 - ac / 2, y / 2, x / 2 - acFree, y / 2), width=acWidth, fill=acColor)
                    draw_pil_im.line((x / 2 + ac / 2, y / 2, x / 2 + acFree, y / 2), width=acWidth, fill=acColor)
                    draw_pil_im.line((x / 2, y / 2 - ac / 2, x / 2, y / 2 - acFree), width=acWidth, fill=acColor)
                    draw_pil_im.line((x / 2, y / 2 + ac / 2, x / 2, y / 2 + acFree), width=acWidth, fill=acColor)
                    draw_pil_im.ellipse((x / 2 - 4 / 2, y / 2 - 4 / 2, x / 2 + 4/ 2, y / 2 + 4 / 2), fill='yellow',
                                        width=1)

                    buf1 = io.BytesIO()
                    pil_im.save(buf1, format='JPEG')
                    self.frame = buf1.getvalue()

                self.condition.notify_all()
            self.buffer.seek(0)
        return self.buffer.write(buf)


class StreamingHandler(server.BaseHTTPRequestHandler):

    def do_GET(self):
        if self.path == '/':
            self.send_response(301)
            self.send_header('Location', '/index.html')
            self.end_headers()
        elif self.path == '/index.html':
            content = PAGE.encode('utf-8')
            self.send_response(200)
            self.send_header('Content-Type', 'text/html')
            self.send_header('Content-Length', len(content))
            self.end_headers()
            self.wfile.write(content)
        elif self.path == '/' + output.token + '/stream.mjpg':
            self.send_response(200)
            self.send_header('Age', 0)
            self.send_header('Cache-Control', 'no-cache, private')
            self.send_header('Pragma', 'no-cache')
            self.send_header('Content-Type', 'multipart/x-mixed-replace; boundary=FRAME')
            self.end_headers()
            token = output.token
            try:
                while True:
                    if token != output.token:
                        self.send_error(404)
                        self.end_headers()
                        break
                    with output.condition:
                        output.condition.wait()
                        frame = output.frame
                    self.wfile.write(b'--FRAME\r\n')
                    self.send_header('Content-Type', 'image/jpeg')
                    self.send_header('Content-Length', len(frame))
                    self.end_headers()
                    self.wfile.write(frame)
                    self.wfile.write(b'\r\n')
            except Exception as e:
                logging.warning(
                    'Removed streaming client %s: %s',
                    self.client_address, str(e))
        elif self.path == ffmpeg_url:
            self.send_response(200)
            self.send_header('Age', 0)
            self.send_header('Cache-Control', 'no-cache, private')
            self.send_header('Pragma', 'no-cache')
            self.send_header('Content-Type', 'multipart/x-mixed-replace; boundary=FRAME')
            self.end_headers()

            try:
                while True:
                    with output.condition:
                        output.condition.wait()
                        frame = output.frame
                    self.wfile.write(b'--FRAME\r\n')
                    self.send_header('Content-Type', 'image/jpeg')
                    self.send_header('Content-Length', len(frame))
                    self.end_headers()
                    self.wfile.write(frame)
                    self.wfile.write(b'\r\n')
            except Exception as e:
                logging.warning(
                    'Removed streaming client %s: %s',
                    self.client_address, str(e))
        else:
            self.send_error(404)
            self.end_headers()


class StreamingServer(socketserver.ThreadingMixIn, server.HTTPServer):
    allow_reuse_address = True
    daemon_threads = True


with picamera.PiCamera(resolution='640x480', framerate=24) as camera:
    output = StreamingOutput()
    camera.zoom = zoom
    # Uncomment the next line to change your Pi's Camera rotation (in degrees)
    # camera.rotation = 90
    # camera.annotate_text = dt.datetime.now().strftime('%Y-%m-%d %H:%M:%S')
    camera.start_recording(output, format='mjpeg')

    while 1 == 1:
        try:
            address = ('', 9092)
            server = StreamingServer(address, StreamingHandler)
            server.serve_forever()

        finally:

            camera.stop_recording()
