#!/bin/bash

sudo kill -9 $(ps aux | grep -e reverse-proxy | awk '{ print $2 }')
sudo kill -9 $(ps aux | grep -e restartCaddy.sh | awk '{ print $2 }')
sudo kill -9 $(ps aux | grep -e server.py | awk '{ print $2 }')
sudo kill -9 $(ps aux | grep -e ffmpeg | awk '{ print $2 }')
sudo kill -9 $(ps aux | grep -e ./stream2 | awk '{ print $2 }')
sudo kill -9 $(ps aux | grep -e server.py | awk '{ print $2 }')
sudo kill -9 $(ps aux | grep -e events.py | awk '{ print $2 }')   
