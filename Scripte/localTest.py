import socketio
speed = 9000
status = ''
allPoints = 0
NewStatus = False
NewPoints = False
AimLastTime =[0,0,0,0,0] 
shootTrigger =False


import time
from secrets import MQTT_SERVER
from shoot import writeToESP
from sendGrbl import home, moveXY,moveX,moveY,hold, resetAlarm
import json
import paho.mqtt.client as mqtt
import os
import subprocess


res = home().decode('utf-8').split(":")
print(res[0])
if  res[0]=="ALARM":
    resetAlarm()
    res = home().decode('utf-8').split(":")
    print(res[0])
    res = home().decode('utf-8').split(":")
    if res[0]=="ALARM":
        exit()

moveXY(50,50,speed)




def on_connect(client, userdata, flags, rc):
    print("Connected with result code {}".format(str(rc)))
    print('Press Ctrl-C to quit.')
    


def on_disconnect(client, userdata, flags, rc):
    if rc != 0:
        print("Unexpected disconnection.")
        exit(0)


def on_message(client, userdata, msg):
    print(msg.topic + " " + str(msg.qos) + " " + str(msg.payload))
    global NewPoints
    global NewStatus
    global allPoints
    global status
    try:
        if (msg.topic=="sensor/Move"):
            status = msg.payload.decode('utf-8')
            print(status)
            NewStatus=True
            
            
       
        print("Fertig")
    except json.JSONDecodeError as e:
        print("Error decoding MQTT message: {}\n{}".format(e, msg.payload))

    except KeyError as e:
        print("MQTT message did not contain required key: {}\n{}".format(e, msg.payload))

    except AssertionError as e:
        print("MQTT message incorrect: {}\n{}".format(e, msg.payload))

    except Exception:
        print(traceback.format_exc())


def on_log(client, mqttc, obj, level, string):
    print(string)





#sio.connect('http://188.34.152.96:3000')


hostname = MQTT_SERVER  # add secrets.py file to project dir with content: MQTT_SERVER = '<example>'
response = 1

    # Test connection to MQTT-Broker
while response != 0:
    print("Trying to reach MQTT-Broker at {}...".format(hostname))
    response = os.system("ping -c 1 " + hostname + " > /dev/null")

    # and then check the response...
    if response != 0:
        print(hostname, 'is Down! Retrying in 5 seconds...')
        time.sleep(5)
print('MQTT-Broker at {} is available.'.format(hostname))
# MQTT-Client setup
client = mqtt.Client()
client.on_connect = on_connect
client.on_disconnect = on_disconnect
client.on_message = on_message

client.connect(hostname, 1883, 60)
client.subscribe("sensor/#", 0)
client.loop_start()

while 1==1:
  if NewStatus:
    NewStatus = False
    x,y = status.split(";")
    x=float(x)
    y=float(y)
    moveXY(x,y,speed)
    time.sleep(1)
    print(writeToESP(100,100,0,0,0,0))
  time.sleep(6)
